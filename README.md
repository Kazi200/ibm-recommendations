# Udacity-IBM-Recommendations

Data Scientist - Term 2 - Project 3

## Project overview
In this project we read in data of users of the IBM Watson Studio platform, perform an exploratory data analysis, and use a variety of methods to determine the most appropriate recommendation of article for a user to see. 

The various types of recommendation will be:
- Rank based recommendations
- Collaborative filtering
- AI-driven recommendations

The details of each of the above, and the results, are discussed in further detail in the notebook itself. 
